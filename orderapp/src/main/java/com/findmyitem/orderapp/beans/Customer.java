package com.findmyitem.orderapp.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table( name = "Customer")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class Customer {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int customerId;

    @Column(name = "customerName")
    private String customerName;

    @Column(name = "customerPassword")
    private String customerPassword;

    @Column(name = "customerZipCode")
    private long customerZipCode;

}

