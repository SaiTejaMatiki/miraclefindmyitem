package com.findmyitem.orderapp.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table( name = "items")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class Items {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int itemId;

    @Column(name = "itemName")
    private String itemName;

    @Column(name = "itemQuantity")
    private String itemQuantity;

    @Column(name = "itemPrice")
    private String itemPrice;

    @Column(name = "storeId")
    private long storeID;
}

