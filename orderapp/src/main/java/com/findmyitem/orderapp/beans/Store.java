package com.findmyitem.orderapp.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table( name = "store")
@NoArgsConstructor
@AllArgsConstructor
@Data

public class Store {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int storeId;

    @Column(name = "storeName")
    private String storeName;

    @Column(name = "storePassword")
    private String storePassword;

    @Column(name = "Item")
    private String item;

    @Column(name = "storeZipCode")
    private long storeZipCode;
}

