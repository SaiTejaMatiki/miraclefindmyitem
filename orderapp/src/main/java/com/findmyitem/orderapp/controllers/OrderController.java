package com.findmyitem.orderapp.controllers;

import com.findmyitem.orderapp.beans.Admin;
import com.findmyitem.orderapp.beans.Customer;
import com.findmyitem.orderapp.beans.Items;
import com.findmyitem.orderapp.beans.Store;
import com.findmyitem.orderapp.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class OrderController {

    @Autowired
    private OrderService orderService;

    //get All Items
    @GetMapping("/items")
    public List<Items> getAllItems(){
        return orderService.getAllItems();
    }

    //get All customers
    @GetMapping("/customers")
    public List<Customer> getAllCustomer(){
        return orderService.getAllCustomer();
    }

    //get All Admins
    @GetMapping("/admins")
    public List<Admin> getAllAdmins(){
        return orderService.getAllAdmins();
    }

    //get All stores
    @GetMapping("/stores")
    public List<Store> getAllStores(){
        return orderService.getAllStores();
    }

    @GetMapping("/admin/{id}")
    public ResponseEntity<Admin> getAdminById(@PathVariable int id) {
        return orderService.getAdminById(id);
    }

    @PostMapping("/addAdmin")
    public Admin createAdmin(@RequestBody Admin admin) {
        return orderService.createAdmin(admin);
    }

    @PostMapping("/addStore")
    public Store createStore(@RequestBody Store store) {
        return orderService.createStore(store);
    }
    @PostMapping("/addItems")
    public Items createItems(@RequestBody Items items) {
        return orderService.createItems(items);
    }
    @PostMapping("/addCustomer")
    public Customer createCustomer(@RequestBody Customer customer) {
        return orderService.createCustomer(customer);
    }

    @PutMapping("/updateAdmin/{id}")
    public ResponseEntity<Admin> updateAdmin(@PathVariable int id, @RequestBody Admin AdminDetails) {
        return orderService.updateAdmin(id, AdminDetails);
    }

    @PutMapping("/updateStore/{id}")
    public ResponseEntity<Store> updateStore(@PathVariable int id, @RequestBody Store StoreDetails) {
        return orderService.updateStore(id, StoreDetails);
    }

    @PutMapping("/updateItems/{id}")
    public ResponseEntity<Items> updateItems(@PathVariable int id, @RequestBody Items ItemsDetails) {
        return orderService.updateItems(id, ItemsDetails);
    }

    @PutMapping("/updateCustomer/{id}")
    public ResponseEntity<Customer> updateCustomer(@PathVariable int id, @RequestBody Customer CustomerDetails) {
        return orderService.updateCustomer(id, CustomerDetails);
    }

    @DeleteMapping("/items/{id}")
    public ResponseEntity<Items> deleteItems(@PathVariable int id)
    {
        return orderService.deleteItems(id);
    }

    @DeleteMapping("/admin/{id}")
    public ResponseEntity<Admin> deleteAdmin(@PathVariable int id)
    {
        return orderService.deleteAdmin(id);
    }

    @DeleteMapping("/customer/{id}")
    public ResponseEntity<Customer> deleteCustomer(@PathVariable int id)
    {
        return orderService.deleteCustomer(id);
    }

    @DeleteMapping("/store/{id}")
    public ResponseEntity<Store> deleteStore(@PathVariable int id)
    {
        return orderService.deleteStore(id);
    }

}
