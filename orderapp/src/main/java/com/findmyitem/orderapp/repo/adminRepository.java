package com.findmyitem.orderapp.repo;

import com.findmyitem.orderapp.beans.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface adminRepository extends JpaRepository<Admin, Integer> {
}