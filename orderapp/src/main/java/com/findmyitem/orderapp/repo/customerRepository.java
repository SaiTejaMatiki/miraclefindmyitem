package com.findmyitem.orderapp.repo;

import com.findmyitem.orderapp.beans.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface customerRepository extends JpaRepository<Customer, Integer> {
}
