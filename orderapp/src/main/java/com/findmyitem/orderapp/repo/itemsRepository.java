package com.findmyitem.orderapp.repo;

import com.findmyitem.orderapp.beans.Items;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface itemsRepository extends JpaRepository<Items, Integer> {
}
