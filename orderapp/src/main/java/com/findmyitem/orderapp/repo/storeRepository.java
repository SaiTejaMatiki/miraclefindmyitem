package com.findmyitem.orderapp.repo;

import com.findmyitem.orderapp.beans.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface storeRepository extends JpaRepository<Store, Integer> {
}