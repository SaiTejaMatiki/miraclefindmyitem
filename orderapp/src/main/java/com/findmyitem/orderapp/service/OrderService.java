package com.findmyitem.orderapp.service;


import com.findmyitem.orderapp.beans.Admin;
import com.findmyitem.orderapp.beans.Customer;
import com.findmyitem.orderapp.beans.Items;
import com.findmyitem.orderapp.beans.Store;
import com.findmyitem.orderapp.repo.adminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import com.findmyitem.orderapp.beans.Customer;

import java.util.List;

public interface OrderService {

    public List<Admin> getAllAdmins();

    public ResponseEntity<Admin> getAdminById(int id);

    public Admin createAdmin(Admin admin);

    public ResponseEntity<Admin> updateAdmin(int id, Admin AdminDetails);

    public ResponseEntity<Admin> deleteAdmin(int id);


    public List<Customer> getAllCustomer();

    public Customer createCustomer(Customer customer);

    public ResponseEntity<Customer> updateCustomer(int id, Customer CustomerDetails);

    public ResponseEntity<Customer> deleteCustomer(int id);


    public List<Store> getAllStores();

    public Store createStore(Store store);

    public ResponseEntity<Store> updateStore(int id, Store StoreDetails);

    public ResponseEntity<Store> deleteStore(int id);


    public List<Items> getAllItems();

    public Items createItems(Items items);

    public ResponseEntity<Items> updateItems(int id, Items ItemsDetails);

    public ResponseEntity<Items> deleteItems(int id);

}
