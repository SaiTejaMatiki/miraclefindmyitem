package com.findmyitem.orderapp.service;

import com.findmyitem.orderapp.Exception.ResourceNotFoundException;
import com.findmyitem.orderapp.beans.Admin;
import com.findmyitem.orderapp.beans.Customer;
import com.findmyitem.orderapp.beans.Items;
import com.findmyitem.orderapp.beans.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private com.findmyitem.orderapp.repo.adminRepository adminRepository;
    @Autowired
    private com.findmyitem.orderapp.repo.customerRepository customerRepository;
    @Autowired
    private com.findmyitem.orderapp.repo.itemsRepository itemsRepository;
    @Autowired
    private com.findmyitem.orderapp.repo.storeRepository storeRepository;

    @Override
    public List<Admin> getAllAdmins() {
        System.out.println("GOT HERE");
        return adminRepository.findAll();
    }

    @Override
    public ResponseEntity<Admin> getAdminById(int id) {
        Admin admin = adminRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Admin does not Exist with ID: "+id));
        return ResponseEntity.ok(admin);
    }

    @Override
    public Admin createAdmin(Admin admin) {
        return adminRepository.save(admin);
    }

    @Override
    public ResponseEntity<Admin> updateAdmin(int id, Admin AdminDetails) {
        Admin admin = adminRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Admin does not exist with ID:" + id));

        admin.setAdminName(AdminDetails.getAdminName());
        admin.setAdminPassword(AdminDetails.getAdminPassword());
        adminRepository.save(admin);
        return ResponseEntity.ok(admin);
    }

    @Override
    public ResponseEntity<Admin> deleteAdmin(int id) {
        Admin admin = adminRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Admin does not Exist with id:" +id));
        adminRepository.delete(admin);
        return ResponseEntity.ok(admin);
    }

    @Override
    public List<Customer> getAllCustomer() {
        System.out.println("GOT HERE");
        return customerRepository.findAll();
    }

    @Override
    public Customer createCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public ResponseEntity<Customer> updateCustomer(int id, Customer CustomerDetails) {
        Customer customer = customerRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Customer does not exist with ID:" + id));

        customer.setCustomerName(CustomerDetails.getCustomerName());
        customer.setCustomerPassword(CustomerDetails.getCustomerPassword());
        customerRepository.save(customer);
        return ResponseEntity.ok(customer);
    }

    @Override
    public ResponseEntity<Customer> deleteCustomer(int id) {
        Customer customer = customerRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Customer does not Exist with id:" +id));
        customerRepository.delete(customer);
        return ResponseEntity.ok(customer);
    }

    @Override
    public List<Store> getAllStores() {
        System.out.println("GOT HERE");
        return storeRepository.findAll();
    }

    @Override
    public Store createStore(Store store) {
        return storeRepository.save(store);
    }

    @Override
    public ResponseEntity<Store> updateStore(int id, Store StoreDetails) {
        Store store = storeRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Store does not exist with ID:" + id));

        store.setStoreName(StoreDetails.getStoreName());
        store.setStorePassword(StoreDetails.getStorePassword());
        storeRepository.save(store);
        return ResponseEntity.ok(store);
    }

    @Override
    public ResponseEntity<Store> deleteStore(int id) {
        Store store = storeRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Store does not Exist with id:" +id));
        storeRepository.delete(store);
        return ResponseEntity.ok(store);
    }

    @Override
    public List<Items> getAllItems() {

        return itemsRepository.findAll();
    }

    @Override
    public Items createItems(Items items) {
        return itemsRepository.save(items);
    }

    @Override
    public ResponseEntity<Items> updateItems(int id, Items ItemsDetails) {
        Items items = itemsRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Item does not exist with ID:" + id));

        items.setItemName(ItemsDetails.getItemName());
        items.setItemQuantity(ItemsDetails.getItemQuantity());
        items.setItemPrice(ItemsDetails.getItemPrice());
        itemsRepository.save(items);
        return ResponseEntity.ok(items);
    }

    @Override
    public ResponseEntity<Items> deleteItems(int id) {
        Items items = itemsRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Item does not Exist with id:" +id));
        itemsRepository.delete(items);
        return ResponseEntity.ok(items);
    }


}
